﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ClickTalpa : MonoBehaviour
{
    [SerializeField]
    Animator animator;
    [SerializeField]
    AudioSource colpita, scappata;

    GameObject[] vite = new GameObject[3];
    float[] coox = { -3.0f, -1.5f, 0.0f, 1.5f, 3.0f };

    private static System.Random rnd = new System.Random();
    void OnMouseDown()
    {
        colpita.Play();
        if (PlayerPrefs.GetInt("Pause") == 0) { 
        ScoreScript.incrScore();
        //GameObject go= GameObject.FindWithTag("Player");
        //float x = rnd.Next(-3, 3);
        //float y = rnd.Next(-3, 4);
        //Instantiate(go, new Vector3(x, y, -2), go.transform.rotation);
        PlayerPrefs.SetInt("cash", PlayerPrefs.GetInt("cash")+1);
        animator.SetFloat("dead", 1);
        GetComponent<ParticleSystem>().enableEmission = true;
        //Destroy(go);
        }
    }

    void lostLife()
    {
        scappata.Play();
        Camera.main.GetComponent<SpawnerTalpa>().Invoke("lifeminus", 0.1f);

    }
    /*static void GameOver()
    {
        SceneManager.LoadScene("GameOverMenu");
        //Instantiate(GameOverMenu, new Vector3(0, 0, -2), GameOverMenu.transform.rotation);
        //GameOverMenu.SetActive(true);
        //GameObject.FindWithTag("GameOverMenu").SetActive(true);
    }*/

    void MoleDead()
    {
        scappata.Play();
        GameObject go = GameObject.FindWithTag("Player");
        animator.SetFloat("dead", 0);
        Destroy(go);
        Camera.main.GetComponent<SpawnerTalpa>().Invoke("spawnNew", 0.1f);


    }

    public void Start()
    {
        GetComponent<ParticleSystem>().enableEmission = false;      //disabilitare emissione particelle all'inizio

    }
    void Update()
    {
        
    }
}
