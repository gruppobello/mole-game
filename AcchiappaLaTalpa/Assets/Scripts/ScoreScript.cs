﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    private static int scorePoint = 0;

    internal static void decrScore(int c)
    {
        scorePoint-=c;
        if (scorePoint < 0)
            scorePoint = 0;
    }

    internal static void setScore(int v)
    {
        scorePoint = v;
    }

    [SerializeField]
    Text score;
    void Start()
    {
        if (!PlayerPrefs.HasKey("HighScore"))
            PlayerPrefs.SetInt("HighScore", 0);
    }


    void Update()
    {
        score.text = "Score : " + scorePoint;
    }

    public static void incrScore()
    {
        scorePoint++;
    }
    public static int getScore()
    {
        return scorePoint;
    }
}
