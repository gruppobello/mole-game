﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreScript : MonoBehaviour
{
    [SerializeField]
    Text testo;
    void Start()
    {
        if (ScoreScript.getScore() > PlayerPrefs.GetInt("HighScore"))
            PlayerPrefs.SetInt("HighScore", ScoreScript.getScore());
    }

    void Update()
    {
        testo.text = "HighScore : " + PlayerPrefs.GetInt("HighScore");
    }
}
