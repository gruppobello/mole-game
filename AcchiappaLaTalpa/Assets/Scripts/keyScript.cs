﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keyScript : MonoBehaviour
{
    [SerializeField]
    AudioSource sound;
    private void Start()
    {
        if (!PlayerPrefs.HasKey("EasterEgg"))
            PlayerPrefs.SetInt("EasterEgg", 0);
    }

    private void OnMouseDown()
    {
        PlayerPrefs.SetInt("EasterEgg", 1);
        sound.Play();
    }


    
}
