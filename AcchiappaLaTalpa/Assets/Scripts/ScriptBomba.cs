﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptBomba : MonoBehaviour
{
    [SerializeField]
    Animator animator;
    [SerializeField]
    AudioSource audio;

    private void OnMouseDown()
    {
        audio.Play();
        if (PlayerPrefs.GetInt("Pause") == 0)
        {
            ScoreScript.decrScore(10);
            animator.SetInteger("Cliccata", 1);
        }
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void Scompare()
    {
        Camera.main.GetComponent<SpawnerTalpa>().Invoke("spawnNew", 0.1f);
        GameObject go= GameObject.FindWithTag("Bomba");
        animator.SetInteger("Cliccata", 0);
        Destroy(go);

    }
}
