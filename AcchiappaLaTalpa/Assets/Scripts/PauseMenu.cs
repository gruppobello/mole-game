﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [SerializeField]
    GameObject key;

    bool isPause = false;
    void Start()
    {
        
    }

    public void click()
    {
        if (isPause==true)
            resume();
        else
            pause();
    }

    public void resume()
    {
        isPause = false;
        Time.timeScale = 1.0f;
        PlayerPrefs.SetInt("Pause", 0);
    }

    public void pause()
    {
        PlayerPrefs.SetInt("Pause", 1);
        isPause = true;
        Time.timeScale=0.0f;        //frezza il gioco

        checkEasterEgg();
    }

    public void checkEasterEgg()
    {
        if (ScoreScript.getScore() > PlayerPrefs.GetInt("HighScore") && ScoreScript.getScore() > 40)
            key.SetActive(true);


    }

    public void resetEasterEgg()
    {
        PlayerPrefs.SetInt("EasterEgg", 0);
    }
}
