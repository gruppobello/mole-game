﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    public void Restart()
    {
        SceneManager.LoadScene("Gioco"); //nome della scene
        ScoreScript.setScore(0);

    }
    public void MainMenu()
    {
        SceneManager.LoadScene("Menu"); //nome della scene
        ScoreScript.setScore(0);

    }

}
