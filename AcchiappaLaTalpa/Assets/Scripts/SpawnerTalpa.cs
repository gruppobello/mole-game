﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnerTalpa : MonoBehaviour
{
    [SerializeField]    //Per mostrare la variabile nell'inspector
    GameObject talpa;
    [SerializeField]
    GameObject bomba;
    [SerializeField]
    GameObject buco;
    [SerializeField]
    GameObject cuore;
    [SerializeField]
    AudioSource audioBuyLife;

    GameObject c1, c2, c3;
    int vita = 3;
    GameObject[] vite = new GameObject[3];
    float[] coox = { -1.5f, 0.0f, 1.5f };

    private static System.Random rnd= new System.Random();
    private float x,y;
    void Start()
    {
        Time.timeScale = 1f;
        PlayerPrefs.SetInt("Pause", 0);
        if (!PlayerPrefs.HasKey("cash"))
            PlayerPrefs.SetInt("cash", 0);

        for (float i = -1.5f; i < 2; i=i+1.5f)
            for (int j = -3; j < 4; j++)
                Instantiate(buco, new Vector3(i, j, -1), buco.transform.rotation);
        spawnNew();

        float xc= -1.9f, yc= 5.5f;
        c1 = Instantiate(cuore, new Vector3(xc, yc, 0), cuore.transform.rotation);             //posizioni float x.nf
        c2 = Instantiate(cuore, new Vector3(-1.3f, yc, 0), cuore.transform.rotation);
        c3 = Instantiate(cuore, new Vector3(-0.7f, yc, 0), cuore.transform.rotation);
        vite[0] = c1;
        vite[1] = c2;
        vite[2] = c3;
    }

    void Update()
    {
        if(PlayerPrefs.GetInt("Pause")==0)
            if (ScoreScript.getScore() >= 200)
            {
                Time.timeScale = 2f;
            }
            else
                if (ScoreScript.getScore() >= 100)
                {
                    Time.timeScale = 1.85f;
                }
                else
                    if (ScoreScript.getScore() >= 75)
                        {
                            Time.timeScale=1.7f;
                        }
                        else
                            if (ScoreScript.getScore() >= 50)
                            {
                                Time.timeScale = 1.5f;
                            }
                            else
                                if (ScoreScript.getScore() >= 25)
                                {
                                    Time.timeScale = 1.2f;
                                }
    }

    void lifeminus() 
    {
        if (vita == 1)
        {
            this.GameOver();
        }
        else
        {
            vite[vita - 1].GetComponent<CuoreScript>().Invoke("setGrey", 0.1f);
            vita--;
            GameObject go = GameObject.FindWithTag("Player");
            float x = rnd.Next(-3, 3);
            float y = rnd.Next(-3, 4);
            int xx = rnd.Next(0, 4);
            this.spawnNew();
            Destroy(go);
        }
    }
    void GameOver()
    {
        SceneManager.LoadScene("GameOverMenu");
    }


    public void spawnNew()
    {
        int xx = rnd.Next(0, 2);
        y = rnd.Next(-3, 4);
        int i = 0;
        if (rnd.Next(0, 4) < rnd.Next(0, 15))
        {
            Instantiate(talpa, new Vector3(coox[xx], y, -1), talpa.transform.rotation);
        }
        else
        {
            Instantiate(bomba, new Vector3(coox[xx], y, -1), talpa.transform.rotation);
        }
        
    }


    void tryBuyLife()
    {
        if (vita != 3 && PlayerPrefs.GetInt("cash")>=50)
        {
            vite[vita++].GetComponent<CuoreScript>().Invoke("setRed", 0.1f);
            PlayerPrefs.SetInt("cash", PlayerPrefs.GetInt("cash") - 50);
            audioBuyLife.Play();
        }
    }

}
