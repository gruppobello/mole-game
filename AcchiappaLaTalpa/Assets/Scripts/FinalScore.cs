﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalScore : MonoBehaviour
{
    [SerializeField]
    Text text;

    void Start()
    {
        text.text = "Score : " + ScoreScript.getScore();
    }
}
